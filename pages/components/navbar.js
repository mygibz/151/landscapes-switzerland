import { useRouter } from 'next/router';
import Link from 'next/link';
import styles from '../../styles/Navbar.module.scss'

const menuItems = [{
    name: "Home",
    link: "/"
},
{
    name: "Gallery",
    link: "/gallery"
},
{
    name: "About",
    link: "/about"
},
{
    name: "Contact",
    link: "/contact"
}].reverse();

function Page() { 
    const router = useRouter();
    
    return (
    <nav className={styles.top}>
        <ul>
            <li className="left">
                <a href="/">Landscapes Switzerland</a>
            </li>
            {menuItems?.map((x, i) => (
                <li key={i}>
                    <Link href={x.link}> 
                    <a className={router.pathname == x.link ? "active" : ""}>{x.name}</a>
                    </Link>
                </li>
            ))}
        </ul>
    </nav>
)};

export default Page