import styles from '../../styles/Footer.module.scss'

const Footer = () => (
    <footer className={styles.main}>
        <div>
            Created By
            <a href="https://landscapes-swiss.ch/" target="blank" >
                Luisa Furrer
            </a>
            and
            <a href="https://ant.lgbt/" target="blank" >
                Yanik Ammann
            </a>
        </div>
    </footer>
)

export default Footer;