import { MongoClient } from 'mongodb';

import nextConnect from 'next-connect';

// const client = new MongoClient('mongodb+srv://Root:admin@cluster0.motcb.mongodb.net/LandscapesSwitzerland?retryWrites=true&w=majority', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// });

// async function database(req, res, next) {
//   if (!client.isConnected()) await client.connect();
//   req.dbClient = client;
//   req.db = client.db('LandscapesSwitzerland');
//   return next();
// }

export async function connectToDatabase() {
  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }

  let promise = MongoClient.connect('mongodb+srv://Root:admin@cluster0.motcb.mongodb.net/LandscapesSwitzerland?retryWrites=true&w=majority', opts).then(client => {
    return {
      client,
      db: client.db('LandscapesSwitzerland'),
    }
  })
  return await promise
}

// const middleware = nextConnect();
// middleware.use(database);
// export default middleware;
export default connectToDatabase;
