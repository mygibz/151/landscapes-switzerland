import Head from 'next/head'
import Navbar from './components/navbar'
import Footer from './components/footer'
import styles from '../styles/Home.module.scss'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Landscapes Switzerland</title>
      </Head>

      <Navbar />

      <main>
        <div className={styles.title}>Landscapes Switzerland</div>
        <div className={styles.description}>Photographs of various Swiss landscapes</div>
      </main>
      
      <Footer />
    </div>
  )
}
