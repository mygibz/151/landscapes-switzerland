import Head from 'next/head'
import { useForm } from "react-hook-form"
import Navbar from './components/navbar'
import Footer from './components/footer'
import styles from '../styles/Contact.module.scss'

const sendEmail = () => {
    const name    = document.getElementsByName("name")[0].value;
    const email   = document.getElementsByName("email")[0].value;
    const message = document.getElementsByName("message")[0].value;
    console.log(name)

    const body = new FormData
    body.append("from", `${name} <test@sandbox09888fae610844d2a98546c25483bdad.mailgun.org>`)
    body.append("to", "hosting@ant.lgbt")
    body.append("to", "kontakt@landscapes-swiss.ch")
    body.append("h:Reply-To", email)
    body.append("subject", "Message from Landscapes-Swiss")
    body.append("text", message)

    fetch("https://api.mailgun.net/v3/sandbox09888fae610844d2a98546c25483bdad.mailgun.org/messages", {
        method: "POST",
        body: body,
        headers: {'Authorization': `Basic YXBpOmY1ZmUzNzUxMWE4YzE1MjZhOGU0M2FlNDAwMTU2ZTFiLTU2NDViMWY5LWFhYWFhYmZl`}
    })
    .then(() => location.href = "/")

    return true;
}

export default function Home() {
  const { handleSubmit } = useForm();
  return (
    <div className={styles.container}>
      <Head>
        <title>Contact</title>
      </Head>

      <Navbar />
        <main>
            <div className={styles.formBackground}>
                <div>
                    <content>
                        <h2 className={styles.title}>Contact form</h2>
                        <form action="" className="contact-form" onSubmit={handleSubmit(sendEmail)}>
                            <div>
                                <input type="text" name="name" placeholder="Name" className="contactFormInput" required />
                            </div>
                            <div>
                                <input type="email" name="email" placeholder="E-Mail" className="contactFormInput" required />
                            </div>
                            <div>
                                <textarea name="message" className="contactFormInput" placeholder="Message" required></textarea>
                            </div>
                            <div>
                                <button type="submit">Send</button>
                            </div>
                        </form>
                    </content>
                    <aside>
                        <h2 className={styles.title}>Social Media</h2>
                        <div>
                            <div>
                                <img src="/instagram.png" />
                                <a className={styles.description} href="https://www.instagram.com/landscapes.swiss/" target="blank" >Landscapes.Swiss</a>
                            </div>
                            <br />
                            <div>
                                <img src="/facebook.png" />
                                <a className={styles.description} href="https://www.facebook.com/landscapes.swiss" target="blank" >Landscapes.Swiss</a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </main>
      <Footer />
    </div>
  )
}
