# Landscapes Switzerland

This is a web-application, written in Next.JS, which functions as a website to show the user images of beautiful **Swiss**-mountain landscapes.

![The Landscapes Switzerland home page](.screenshot.png)

You can enjoy a live version of this application here: [landscapes-swiss.ch](https://landscapes-swiss.ch/) (url is temporary).