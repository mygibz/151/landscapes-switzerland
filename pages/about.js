import Head from 'next/head'
import Navbar from './components/navbar'
import Footer from './components/footer'

import styles from '../styles/About.module.scss'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>About</title>
      </Head>

      <Navbar />

      <main>
        <img src="favicon.png"/>
        <section>
          <div className={styles.title}>Hi there!</div>
          <p>This website, written in Next.js, was created as a school project. It should replace the current website on the domain <a href="https://landscapes-swiss.ch/" target="blank" >landscapes-swiss.ch</a> as soon as possible.</p>
          <p>
            Enough technicalities, now go and look at some swiss landscapes! :)
          </p>
        </section>
      </main>
      
      <Footer />
    </div>
  )
}
