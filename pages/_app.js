import '../styles/globals.scss'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  return (<>
    <Head>
      <meta name="description" content="A place for photographs of various Swiss landscapes" />
      <link rel="icon" href="/favicon.png" />
    </Head>
    <Component {...pageProps} />
    </>)
}

export default MyApp
