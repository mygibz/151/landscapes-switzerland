import Head from 'next/head'
import Navbar from './components/navbar'
import Footer from './components/footer'

import { connectToDatabase } from './api/_database'

import styles from '../styles/Gallery.module.scss'
import { server } from '../config'

import ImageZoom from 'react-medium-image-zoom'

const Gallery = ({ images }) => {
  return (
    <div>
      <Head>
        <title>Gallery</title>
      </Head>

      <Navbar />

      <main>
        <div className={styles.grid}>
          {images.map(({ _id, full_src, thumb_src, alt }) => (
            <div key={_id}>
              <ImageZoom
                image={{
                  src: thumb_src,
                  alt: alt
                }}
                zoomImage={{
                  src: full_src,
                  alt: alt
                }}
              />
              <span>{alt}</span>
            </div>
          ))}
        </div>
      </main>
      <Footer />
    </div>
  )
}

export default Gallery;

export async function getStaticProps() {
  // const res = await fetch(`${server}/api/images`);
  
  const { db } = await connectToDatabase();
  const images = await db.collection('Posts').find().toArray()

  return {
    props: {
      images: JSON.parse(JSON.stringify(images)),
    },
    revalidate: 300
  };
}
